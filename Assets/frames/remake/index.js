const { createCanvas, loadImage } = require('canvas')
const subImageMatch = require("./match-subimage");
const PNG = require("pngjs").PNG;
const fs = require('fs')
const canvas = createCanvas(256, 224)
const ctx = canvas.getContext('2d')

// var autofindData = new Buffer();
// var frameData = new Buffer();
// fs.writeFile('autofind.bmp', autofindData, 'binary', function (err) {
//   fs.writeFile('frame.jpg', frameData, 'binary', function (err) {
//     const result = subImageMatch(autofindData, frameData);
//     console.log(result);
//   })
// })

const autofindData = PNG.sync.read(fs.readFileSync("autofind.png"));
const frameData = PNG.sync.read(fs.readFileSync("frame.png"));

  const result = subImageMatch(frameData, autofindData);
      console.log(result);


loadImage('frame.jpg').then((image) => {
  ctx.drawImage(image, 0, 0);
  
// ctx.font = '30px Impact'
// ctx.rotate(0.1)
// ctx.fillText('Awesome!', 50, 100)

// var text = ctx.measureText('Awesome!')
// ctx.strokeStyle = 'rgba(0,0,0,0.5)'
// ctx.beginPath()
// ctx.lineTo(50, 102)
// ctx.lineTo(50 + text.width, 102)
// ctx.stroke()

  const out = fs.createWriteStream(__dirname + '/test.png')
  const stream = canvas.createPNGStream()
  stream.pipe(out)
  out.on('finish', () =>  console.log('The PNG file was created.'))
})