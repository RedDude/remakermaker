﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommandLine;
using RemakerMaker.CLI;

namespace RemakerMaker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                CommandLine.Parser.Default.ParseArguments<Options>(args)
                    .WithParsed(Options.RunOptions)
                    .WithNotParsed(Options.HandleParseError);
                return;
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrame());
        }
    }
}
