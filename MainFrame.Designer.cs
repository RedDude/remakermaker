﻿namespace RemakerMaker
{
    partial class MainFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.finshTime = new System.Windows.Forms.Label();
            this.fPadY = new System.Windows.Forms.NumericUpDown();
            this.fPadX = new System.Windows.Forms.NumericUpDown();
            this.ftileY = new System.Windows.Forms.NumericUpDown();
            this.ftileX = new System.Windows.Forms.NumericUpDown();
            this.fOffsetY = new System.Windows.Forms.NumericUpDown();
            this.foffsetX = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bLockGrid = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fpicHeight = new System.Windows.Forms.TextBox();
            this.fpicWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mainPictureBox = new System.Windows.Forms.PictureBox();
            this.autoFindGrid = new System.Windows.Forms.Button();
            this.autoFindPictureBox = new System.Windows.Forms.PictureBox();
            this.autoExtractCheckBox = new System.Windows.Forms.CheckBox();
            this.totalExtractedLabel = new System.Windows.Forms.Label();
            this.currentMap = new System.Windows.Forms.RichTextBox();
            this.show2DArray = new System.Windows.Forms.CheckBox();
            this.autoIgnoreTooManyTiles = new System.Windows.Forms.CheckBox();
            this.playerBar = new System.Windows.Forms.TrackBar();
            this.gridTileMemento = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.selectedTilePictureBox = new System.Windows.Forms.PictureBox();
            this.TopNeighborsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.RightNeighborsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.DownNeighborsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.LeftNeighborsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.topLabel = new System.Windows.Forms.Label();
            this.rightLabel = new System.Windows.Forms.Label();
            this.downLabel = new System.Windows.Forms.Label();
            this.leftLabel = new System.Windows.Forms.Label();
            this.tileInfoLabel = new System.Windows.Forms.Label();
            this.tileIdLabel = new System.Windows.Forms.Label();
            this.ignoreOnSearch = new System.Windows.Forms.CheckBox();
            this.deleteTileButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.fPadY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.fPadX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.ftileY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.ftileX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.fOffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.foffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.mainPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.autoFindPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.playerBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.selectedTilePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(129, 201);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 29);
            this.button3.TabIndex = 45;
            this.button3.Text = "findGrid";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.findGrid_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(21, 514);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1456, 194);
            this.flowLayoutPanel1.TabIndex = 43;
            // 
            // finshTime
            // 
            this.finshTime.AutoSize = true;
            this.finshTime.Location = new System.Drawing.Point(363, 482);
            this.finshTime.Name = "finshTime";
            this.finshTime.Size = new System.Drawing.Size(73, 20);
            this.finshTime.TabIndex = 42;
            this.finshTime.Text = "finshTime";
            // 
            // fPadY
            // 
            this.fPadY.Location = new System.Drawing.Point(88, 375);
            this.fPadY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fPadY.Name = "fPadY";
            this.fPadY.Size = new System.Drawing.Size(60, 27);
            this.fPadY.TabIndex = 41;
            this.fPadY.Value = new decimal(new int[] {80, 0, 0, 0});
            // 
            // fPadX
            // 
            this.fPadX.Location = new System.Drawing.Point(21, 374);
            this.fPadX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fPadX.Name = "fPadX";
            this.fPadX.Size = new System.Drawing.Size(60, 27);
            this.fPadX.TabIndex = 40;
            this.fPadX.Value = new decimal(new int[] {8, 0, 0, 0});
            // 
            // ftileY
            // 
            this.ftileY.Location = new System.Drawing.Point(88, 319);
            this.ftileY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ftileY.Name = "ftileY";
            this.ftileY.Size = new System.Drawing.Size(60, 27);
            this.ftileY.TabIndex = 39;
            this.ftileY.Value = new decimal(new int[] {16, 0, 0, 0});
            // 
            // ftileX
            // 
            this.ftileX.Location = new System.Drawing.Point(21, 319);
            this.ftileX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ftileX.Name = "ftileX";
            this.ftileX.Size = new System.Drawing.Size(60, 27);
            this.ftileX.TabIndex = 38;
            this.ftileX.Value = new decimal(new int[] {16, 0, 0, 0});
            // 
            // fOffsetY
            // 
            this.fOffsetY.Location = new System.Drawing.Point(88, 259);
            this.fOffsetY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fOffsetY.Name = "fOffsetY";
            this.fOffsetY.Size = new System.Drawing.Size(60, 27);
            this.fOffsetY.TabIndex = 37;
            this.fOffsetY.Value = new decimal(new int[] {5, 0, 0, 0});
            // 
            // foffsetX
            // 
            this.foffsetX.Location = new System.Drawing.Point(21, 259);
            this.foffsetX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.foffsetX.Name = "foffsetX";
            this.foffsetX.Size = new System.Drawing.Size(60, 27);
            this.foffsetX.TabIndex = 36;
            this.foffsetX.Value = new decimal(new int[] {1, 0, 0, 0});
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(181, 479);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 29);
            this.button2.TabIndex = 35;
            this.button2.Text = "clean";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.clean_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 479);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 29);
            this.button1.TabIndex = 34;
            this.button1.Text = "save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.save_Click);
            // 
            // bLockGrid
            // 
            this.bLockGrid.Location = new System.Drawing.Point(21, 479);
            this.bLockGrid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bLockGrid.Name = "bLockGrid";
            this.bLockGrid.Size = new System.Drawing.Size(75, 29);
            this.bLockGrid.TabIndex = 33;
            this.bLockGrid.Text = "extract";
            this.bLockGrid.UseVisualStyleBackColor = true;
            this.bLockGrid.Click += new System.EventHandler(this.extract_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 32;
            this.label4.Text = "Padding";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 20);
            this.label3.TabIndex = 31;
            this.label3.Text = "Tile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 30;
            this.label2.Text = "Border";
            // 
            // fpicHeight
            // 
            this.fpicHeight.Location = new System.Drawing.Point(75, 201);
            this.fpicHeight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fpicHeight.Name = "fpicHeight";
            this.fpicHeight.Size = new System.Drawing.Size(48, 27);
            this.fpicHeight.TabIndex = 29;
            this.fpicHeight.Text = "256";
            // 
            // fpicWidth
            // 
            this.fpicWidth.Location = new System.Drawing.Point(21, 201);
            this.fpicWidth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fpicWidth.Name = "fpicWidth";
            this.fpicWidth.Size = new System.Drawing.Size(48, 27);
            this.fpicWidth.TabIndex = 28;
            this.fpicWidth.Text = "203";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Size";
            // 
            // mainPictureBox
            // 
            this.mainPictureBox.Location = new System.Drawing.Point(265, 94);
            this.mainPictureBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainPictureBox.Name = "mainPictureBox";
            this.mainPictureBox.Size = new System.Drawing.Size(357, 374);
            this.mainPictureBox.TabIndex = 26;
            this.mainPictureBox.TabStop = false;
            this.mainPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            // 
            // autoFindGrid
            // 
            this.autoFindGrid.Location = new System.Drawing.Point(12, 18);
            this.autoFindGrid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.autoFindGrid.Name = "autoFindGrid";
            this.autoFindGrid.Size = new System.Drawing.Size(105, 29);
            this.autoFindGrid.TabIndex = 45;
            this.autoFindGrid.Text = "AutoFindGrid";
            this.autoFindGrid.UseVisualStyleBackColor = true;
            this.autoFindGrid.Click += new System.EventHandler(this.autoFindGrid_Click);
            // 
            // autoFindPictureBox
            // 
            this.autoFindPictureBox.Location = new System.Drawing.Point(167, 18);
            this.autoFindPictureBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.autoFindPictureBox.Name = "autoFindPictureBox";
            this.autoFindPictureBox.Size = new System.Drawing.Size(88, 92);
            this.autoFindPictureBox.TabIndex = 26;
            this.autoFindPictureBox.TabStop = false;
            // 
            // autoExtractCheckBox
            // 
            this.autoExtractCheckBox.AutoSize = true;
            this.autoExtractCheckBox.Checked = true;
            this.autoExtractCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoExtractCheckBox.Location = new System.Drawing.Point(15, 59);
            this.autoExtractCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.autoExtractCheckBox.Name = "autoExtractCheckBox";
            this.autoExtractCheckBox.Size = new System.Drawing.Size(146, 24);
            this.autoExtractCheckBox.TabIndex = 46;
            this.autoExtractCheckBox.Text = "Auto Extract Tiles";
            this.autoExtractCheckBox.UseVisualStyleBackColor = true;
            // 
            // totalExtractedLabel
            // 
            this.totalExtractedLabel.AutoSize = true;
            this.totalExtractedLabel.Location = new System.Drawing.Point(261, 482);
            this.totalExtractedLabel.Name = "totalExtractedLabel";
            this.totalExtractedLabel.Size = new System.Drawing.Size(102, 20);
            this.totalExtractedLabel.TabIndex = 47;
            this.totalExtractedLabel.Text = "totalExtracted";
            // 
            // currentMap
            // 
            this.currentMap.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.currentMap.Location = new System.Drawing.Point(1057, 98);
            this.currentMap.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.currentMap.Name = "currentMap";
            this.currentMap.Size = new System.Drawing.Size(431, 215);
            this.currentMap.TabIndex = 49;
            this.currentMap.Text = "";
            // 
            // show2DArray
            // 
            this.show2DArray.AutoSize = true;
            this.show2DArray.Location = new System.Drawing.Point(1360, 322);
            this.show2DArray.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.show2DArray.Name = "show2DArray";
            this.show2DArray.Size = new System.Drawing.Size(129, 24);
            this.show2DArray.TabIndex = 50;
            this.show2DArray.Text = "Show 2D Array";
            this.show2DArray.UseVisualStyleBackColor = true;
            // 
            // autoIgnoreTooManyTiles
            // 
            this.autoIgnoreTooManyTiles.AutoSize = true;
            this.autoIgnoreTooManyTiles.Checked = true;
            this.autoIgnoreTooManyTiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoIgnoreTooManyTiles.Location = new System.Drawing.Point(12, 118);
            this.autoIgnoreTooManyTiles.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.autoIgnoreTooManyTiles.Name = "autoIgnoreTooManyTiles";
            this.autoIgnoreTooManyTiles.Size = new System.Drawing.Size(213, 24);
            this.autoIgnoreTooManyTiles.TabIndex = 51;
            this.autoIgnoreTooManyTiles.Text = "Auto Ignore Too Many Tiles";
            this.autoIgnoreTooManyTiles.UseVisualStyleBackColor = true;
            // 
            // playerBar
            // 
            this.playerBar.Location = new System.Drawing.Point(265, 18);
            this.playerBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.playerBar.Maximum = 1;
            this.playerBar.Minimum = 1;
            this.playerBar.Name = "playerBar";
            this.playerBar.Size = new System.Drawing.Size(1064, 56);
            this.playerBar.TabIndex = 52;
            this.playerBar.Value = 1;
            this.playerBar.ValueChanged += new System.EventHandler(this.playerBar_ValueChanged);
            // 
            // gridTileMemento
            // 
            this.gridTileMemento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gridTileMemento.CausesValidation = false;
            this.gridTileMemento.ColumnCount = 15;
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.gridTileMemento.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.gridTileMemento.Location = new System.Drawing.Point(637, 94);
            this.gridTileMemento.Margin = new System.Windows.Forms.Padding(0);
            this.gridTileMemento.Name = "gridTileMemento";
            this.gridTileMemento.RowCount = 15;
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.gridTileMemento.Size = new System.Drawing.Size(356, 374);
            this.gridTileMemento.TabIndex = 53;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1103, 331);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 35);
            this.button4.TabIndex = 54;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // selectedTilePictureBox
            // 
            this.selectedTilePictureBox.Location = new System.Drawing.Point(23, 749);
            this.selectedTilePictureBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.selectedTilePictureBox.Name = "selectedTilePictureBox";
            this.selectedTilePictureBox.Size = new System.Drawing.Size(88, 92);
            this.selectedTilePictureBox.TabIndex = 55;
            this.selectedTilePictureBox.TabStop = false;
            // 
            // TopNeighborsLayout
            // 
            this.TopNeighborsLayout.AutoScroll = true;
            this.TopNeighborsLayout.Location = new System.Drawing.Point(167, 749);
            this.TopNeighborsLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TopNeighborsLayout.Name = "TopNeighborsLayout";
            this.TopNeighborsLayout.Size = new System.Drawing.Size(1311, 46);
            this.TopNeighborsLayout.TabIndex = 44;
            // 
            // RightNeighborsLayout
            // 
            this.RightNeighborsLayout.AutoScroll = true;
            this.RightNeighborsLayout.Location = new System.Drawing.Point(167, 801);
            this.RightNeighborsLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RightNeighborsLayout.Name = "RightNeighborsLayout";
            this.RightNeighborsLayout.Size = new System.Drawing.Size(1311, 51);
            this.RightNeighborsLayout.TabIndex = 45;
            // 
            // DownNeighborsLayout
            // 
            this.DownNeighborsLayout.AutoScroll = true;
            this.DownNeighborsLayout.Location = new System.Drawing.Point(167, 859);
            this.DownNeighborsLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DownNeighborsLayout.Name = "DownNeighborsLayout";
            this.DownNeighborsLayout.Size = new System.Drawing.Size(1311, 55);
            this.DownNeighborsLayout.TabIndex = 45;
            // 
            // LeftNeighborsLayout
            // 
            this.LeftNeighborsLayout.AutoScroll = true;
            this.LeftNeighborsLayout.Location = new System.Drawing.Point(167, 920);
            this.LeftNeighborsLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LeftNeighborsLayout.Name = "LeftNeighborsLayout";
            this.LeftNeighborsLayout.Size = new System.Drawing.Size(1311, 60);
            this.LeftNeighborsLayout.TabIndex = 45;
            // 
            // topLabel
            // 
            this.topLabel.AutoSize = true;
            this.topLabel.Location = new System.Drawing.Point(125, 754);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(34, 20);
            this.topLabel.TabIndex = 42;
            this.topLabel.Text = "Top";
            // 
            // rightLabel
            // 
            this.rightLabel.AutoSize = true;
            this.rightLabel.Location = new System.Drawing.Point(125, 806);
            this.rightLabel.Name = "rightLabel";
            this.rightLabel.Size = new System.Drawing.Size(44, 20);
            this.rightLabel.TabIndex = 56;
            this.rightLabel.Text = "Right";
            // 
            // downLabel
            // 
            this.downLabel.AutoSize = true;
            this.downLabel.Location = new System.Drawing.Point(121, 862);
            this.downLabel.Name = "downLabel";
            this.downLabel.Size = new System.Drawing.Size(48, 20);
            this.downLabel.TabIndex = 56;
            this.downLabel.Text = "Down";
            // 
            // leftLabel
            // 
            this.leftLabel.AutoSize = true;
            this.leftLabel.Location = new System.Drawing.Point(128, 919);
            this.leftLabel.Name = "leftLabel";
            this.leftLabel.Size = new System.Drawing.Size(34, 20);
            this.leftLabel.TabIndex = 56;
            this.leftLabel.Text = "Left";
            // 
            // tileInfoLabel
            // 
            this.tileInfoLabel.AutoSize = true;
            this.tileInfoLabel.Location = new System.Drawing.Point(21, 726);
            this.tileInfoLabel.Name = "tileInfoLabel";
            this.tileInfoLabel.Size = new System.Drawing.Size(63, 20);
            this.tileInfoLabel.TabIndex = 42;
            this.tileInfoLabel.Text = "Tile Info";
            // 
            // tileIdLabel
            // 
            this.tileIdLabel.AutoSize = true;
            this.tileIdLabel.Location = new System.Drawing.Point(89, 726);
            this.tileIdLabel.Name = "tileIdLabel";
            this.tileIdLabel.Size = new System.Drawing.Size(22, 20);
            this.tileIdLabel.TabIndex = 57;
            this.tileIdLabel.Text = "Id";
            // 
            // ignoreOnSearch
            // 
            this.ignoreOnSearch.AutoSize = true;
            this.ignoreOnSearch.Location = new System.Drawing.Point(7, 888);
            this.ignoreOnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ignoreOnSearch.Name = "ignoreOnSearch";
            this.ignoreOnSearch.Size = new System.Drawing.Size(141, 24);
            this.ignoreOnSearch.TabIndex = 58;
            this.ignoreOnSearch.Text = "Ignore on search";
            this.ignoreOnSearch.UseVisualStyleBackColor = true;
            this.ignoreOnSearch.Click += new System.EventHandler(this.ignoreOnSearch_Click);
            // 
            // deleteTileButton
            // 
            this.deleteTileButton.Location = new System.Drawing.Point(23, 851);
            this.deleteTileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deleteTileButton.Name = "deleteTileButton";
            this.deleteTileButton.Size = new System.Drawing.Size(88, 29);
            this.deleteTileButton.TabIndex = 59;
            this.deleteTileButton.Text = "Delete";
            this.deleteTileButton.UseVisualStyleBackColor = true;
            this.deleteTileButton.Click += new System.EventHandler(this.deleteTileButton_Click);
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1505, 1057);
            this.Controls.Add(this.deleteTileButton);
            this.Controls.Add(this.ignoreOnSearch);
            this.Controls.Add(this.tileIdLabel);
            this.Controls.Add(this.leftLabel);
            this.Controls.Add(this.downLabel);
            this.Controls.Add(this.rightLabel);
            this.Controls.Add(this.LeftNeighborsLayout);
            this.Controls.Add(this.DownNeighborsLayout);
            this.Controls.Add(this.RightNeighborsLayout);
            this.Controls.Add(this.TopNeighborsLayout);
            this.Controls.Add(this.selectedTilePictureBox);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.gridTileMemento);
            this.Controls.Add(this.playerBar);
            this.Controls.Add(this.autoIgnoreTooManyTiles);
            this.Controls.Add(this.show2DArray);
            this.Controls.Add(this.currentMap);
            this.Controls.Add(this.totalExtractedLabel);
            this.Controls.Add(this.autoExtractCheckBox);
            this.Controls.Add(this.autoFindGrid);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tileInfoLabel);
            this.Controls.Add(this.topLabel);
            this.Controls.Add(this.finshTime);
            this.Controls.Add(this.fPadY);
            this.Controls.Add(this.fPadX);
            this.Controls.Add(this.ftileY);
            this.Controls.Add(this.ftileX);
            this.Controls.Add(this.fOffsetY);
            this.Controls.Add(this.foffsetX);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bLockGrid);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fpicHeight);
            this.Controls.Add(this.fpicWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.autoFindPictureBox);
            this.Controls.Add(this.mainPictureBox);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainFrame";
            this.Text = "RemakerMaker";
            this.Load += new System.EventHandler(this._Load);
            ((System.ComponentModel.ISupportInitialize) (this.fPadY)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.fPadX)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.ftileY)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.ftileX)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.fOffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.foffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.mainPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.autoFindPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.playerBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.selectedTilePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label finshTime;
        private System.Windows.Forms.NumericUpDown fPadY;
        private System.Windows.Forms.NumericUpDown fPadX;
        private System.Windows.Forms.NumericUpDown ftileY;
        private System.Windows.Forms.NumericUpDown ftileX;
        private System.Windows.Forms.NumericUpDown fOffsetY;
        private System.Windows.Forms.NumericUpDown foffsetX;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bLockGrid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fpicHeight;
        private System.Windows.Forms.TextBox fpicWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox mainPictureBox;
        private System.Windows.Forms.Button autoFindGrid;
        private System.Windows.Forms.PictureBox autoFindPictureBox;
        private System.Windows.Forms.CheckBox autoExtractCheckBox;
        private System.Windows.Forms.Label totalExtractedLabel;
        private System.Windows.Forms.RichTextBox currentMap;
        private System.Windows.Forms.CheckBox show2DArray;
        private System.Windows.Forms.CheckBox autoIgnoreTooManyTiles;
        private System.Windows.Forms.TrackBar playerBar;
        private System.Windows.Forms.TableLayoutPanel gridTileMemento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox selectedTilePictureBox;
        private System.Windows.Forms.FlowLayoutPanel TopNeighborsLayout;
        private System.Windows.Forms.FlowLayoutPanel RightNeighborsLayout;
        private System.Windows.Forms.FlowLayoutPanel DownNeighborsLayout;
        private System.Windows.Forms.FlowLayoutPanel LeftNeighborsLayout;
        private System.Windows.Forms.Label topLabel;
        private System.Windows.Forms.Label rightLabel;
        private System.Windows.Forms.Label downLabel;
        private System.Windows.Forms.Label leftLabel;
        private System.Windows.Forms.Label tileInfoLabel;
        private System.Windows.Forms.Label tileIdLabel;
        private System.Windows.Forms.CheckBox ignoreOnSearch;
        private System.Windows.Forms.Button deleteTileButton;
    }
}

