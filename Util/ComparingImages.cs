﻿using System.Drawing;
using System.Security.Cryptography;

namespace RemakerMaker.Util
{
    public class ComparingImages
    {
        public enum CompareResult
        {
            ciCompareOk,
            ciPixelMismatch,
            ciSizeMismatch
        };

        public static CompareResult Compare(Bitmap bmp1, Bitmap bmp2)
        {
            var cr = CompareResult.ciCompareOk;

            //Test to see if we have the same size of image
            if (bmp1.Size != bmp2.Size)
            {
                cr = CompareResult.ciSizeMismatch;
            }
            else
            {
                //Convert each image to a byte array
                System.Drawing.ImageConverter ic = 
                    new System.Drawing.ImageConverter();
                var btImage1 = new byte[1];
                btImage1 = (byte[])ic.ConvertTo(bmp1, btImage1.GetType());
                var btImage2 = new byte[1];
                btImage2 = (byte[])ic.ConvertTo(bmp2, btImage2.GetType());
                
                //Compute a hash for each image
                SHA256Managed shaM = new SHA256Managed();
                var hash1 = shaM.ComputeHash(btImage1);
                var hash2 = shaM.ComputeHash(btImage2);

                //Compare the hash values
                for (var i = 0; i < hash1.Length && i < hash2.Length 
                                                 && cr == CompareResult.ciCompareOk; i++)
                {
                    if (hash1[i] != hash2[i])
                        cr = CompareResult.ciPixelMismatch;
                }
            }
            return cr;
        }
        
        public static byte[] CreateHash(Bitmap bmp1)
        {
            var ic = new ImageConverter();
            var btImage = new byte[1];
            btImage = (byte[])ic.ConvertTo(bmp1, btImage.GetType());

            var shaM = new SHA256Managed();
            return shaM.ComputeHash(btImage);
        }

        
        public static bool Compare(byte[] hash, byte[] hash2)
        {
            var cr = CompareResult.ciCompareOk;

            for (var i = 0; i < hash.Length && i < hash2.Length 
                                            && cr == CompareResult.ciCompareOk; i++)
            {
                if (hash[i] != hash2[i])
                    return false;
            }

            return true;
        }
    }
}