﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RemakerMaker.Main;
using RemakerMaker.Properties;

namespace RemakerMaker.Util
{
    class FormHelper
    {
        public static void DrawGrid(ref PaintEventArgs e, int picWidth, int picHeight, int tileX, int tileY, int offsetY, int offsetX)
        {
            Graphics g = e.Graphics;
            int numCellsX = picWidth / tileY;
            int numCellsY = picHeight / tileX;
            int cellSizeY = tileY;
            int cellSizeX = tileX;
            Pen p = new Pen(Color.Red);

            for (int y = 0; y < numCellsX; ++y)
            {
                g.DrawLine(p, 0, y * cellSizeY + offsetY, numCellsY * cellSizeY, y * cellSizeY + offsetY);

            }

            for (int x = 0; x < numCellsY; ++x)
            {
                g.DrawLine(p, x * cellSizeX + offsetX, 0, x * cellSizeX + offsetX, numCellsX * cellSizeX);
            }

        }

        public static void DrawFoundRectangleGrid(ref PaintEventArgs e, int picWidth, int picHeight, int tileX, int tileY, Rectangle foundRectangle)
        {
            Graphics g = e.Graphics;
            int numCellsX = picWidth / tileY;
            int numCellsY = picHeight / tileX;
            int cellSizeY = tileY;
            int cellSizeX = tileX;
            Pen p = new Pen(Color.GreenYellow);
            Pen p2 = new Pen(Color.Yellow);

            for (int y = 0; y < numCellsX; ++y)
            {
                g.DrawLine(p, 0, y * cellSizeY + foundRectangle.Y, numCellsY * cellSizeY, y * cellSizeY + foundRectangle.Y);

                if (y * cellSizeY + foundRectangle.Y > picHeight + cellSizeY *2 )
                {
                    break;
                }
            }

            for (int y = numCellsX; y > 0; --y)
            {
                g.DrawLine(p, 0, (cellSizeY + foundRectangle.Y) - (y * cellSizeY), numCellsY * cellSizeY, (cellSizeY + foundRectangle.Y) - (y * cellSizeY));

            }

            for (int x = 0; x < numCellsY; ++x)
            {
                g.DrawLine(p, x * cellSizeX + foundRectangle.X, 0, x * cellSizeX + foundRectangle.X, numCellsX * cellSizeX);
            
                if (x * cellSizeX + foundRectangle.X > picWidth + cellSizeX *2 )
                {
                    break;
                }
            }

            for (int x = numCellsY; x > 0; --x)
            {
                g.DrawLine(p, (cellSizeX + foundRectangle.X) - (x * cellSizeX), 0, (cellSizeX + foundRectangle.X) - (x * cellSizeX), numCellsX * cellSizeX);

            }

        }

        public static void DrawDetectRet(ref PaintEventArgs e, Rectangle foundRectangle)
        {
            Graphics g = e.Graphics;
            SolidBrush pg = new SolidBrush(Color.Green);
            g.FillRectangle(pg, foundRectangle);

        }

        public static void ExtractTilesBitmaps(ref PaintEventArgs e, Bitmap image, Rectangle canvas, Rectangle tileStart)
        {
            Graphics g = e.Graphics;
            SolidBrush pg = new SolidBrush(Color.BlueViolet);
            SolidBrush pgi = new SolidBrush(Color.Orange);
            SolidBrush pgout = new SolidBrush(Color.DeepPink);
            SolidBrush pgTop = new SolidBrush(Color.Chartreuse);
            Font myFont = new Font("Arial", 10);

            canvas.X = 0;
            canvas.Y = 0;

            int picWidth = canvas.Width;
            int picHeight = canvas.Height;
            decimal num = (picWidth - tileStart.Y) / tileStart.Height;
            int cast = (int)num;

            int numCellsX = cast - 1;

            num = (picWidth - tileStart.X) / tileStart.Width;

            cast = (int)num;
            int numCellsY = cast - 1;

            int index = 0;
            for (int y = 0; y < numCellsY; ++y)
            {
                for (int x = 0;; ++x)
                {
                    var searchRect = new Rectangle(x * tileStart.Width + tileStart.X, y * tileStart.Height + tileStart.Y, tileStart.Width, tileStart.Height);

                    g.FillRectangle(pgTop, searchRect);

                    if (!canvas.Contains(searchRect))
                    {
                        g.FillRectangle(pgout, searchRect);
                        break;
                    }

                }
            }

        }

        public static void FindStartGrid(ref PaintEventArgs e, Bitmap image, Rectangle pic, Rectangle tile)
        {
            Graphics g = e.Graphics;
            SolidBrush pg = new SolidBrush(Color.BlueViolet);
            SolidBrush pgi = new SolidBrush(Color.Orange);
            SolidBrush pgout = new SolidBrush(Color.DeepPink);
            SolidBrush pgTop = new SolidBrush(Color.Chartreuse);
            Font myFont = new Font("Arial", 10);

            pic.X = 0;
            pic.Y = 0;

            var i = 0;

            var y = 1;
            var yi = 1;

            var x = 1;
            var xi = 1;

            var doneY = 0;
            var doneYI = 0;

            var doneX = 0;
            var doneXI = 0;

            var countY = 0;
            var countX = 0;

            while (i < 20)
            {
                i++;

                if (doneY == 0)
                {
                    var searchRect = new Rectangle(tile.X, tile.Y - (tile.Height * y), tile.Width, tile.Height);

                    y++;

                    g.FillRectangle(pg, searchRect);
                    e.Graphics.DrawString(y.ToString(), myFont, Brushes.White, new Point(searchRect.X, searchRect.Y));

                    if (!pic.Contains(searchRect))
                    {
                        g.FillRectangle(pgout, searchRect);



                        doneY = --y;
                    }
                }

                if (doneYI == 0)
                {
                    var searchRect = new Rectangle(tile.X, tile.Y + (tile.Height * yi), tile.Width, tile.Height);

                    yi++;

                    g.FillRectangle(pg, searchRect);
                    e.Graphics.DrawString(yi.ToString(), myFont, Brushes.White, new Point(searchRect.X, searchRect.Y));
                    if (!pic.Contains(searchRect))
                    {
                        g.FillRectangle(pgout, searchRect);
                        doneYI = --yi;

                    }
                }

                if (doneX == 0)
                {
                    var searchRect = new Rectangle(tile.X - (tile.Height * x), tile.Y, tile.Width, tile.Height);

                    x++;

                    g.FillRectangle(pgi, searchRect);
                    e.Graphics.DrawString(x.ToString(), myFont, Brushes.White, new Point(searchRect.X, searchRect.Y));
                    if (!pic.Contains(searchRect))
                    {
                        g.FillRectangle(pgout, searchRect);

                        doneX = --x;
                    }
                }

                if (doneXI == 0)
                {
                    var searchRect = new Rectangle(tile.X + (tile.Height * xi), tile.Y, tile.Width, tile.Height);

                    xi++;

                    g.FillRectangle(pgi, searchRect);
                    e.Graphics.DrawString(xi.ToString(), myFont, Brushes.White, new Point(searchRect.X, searchRect.Y));
                    if (!pic.Contains(searchRect))
                    {
                        g.FillRectangle(pgout, searchRect);

                        doneXI = --xi;

                    }
                }


                if (doneYI > 0 && doneY > 0 && doneXI > 0 && doneX > 0)
                {
                    break;
                }

            }

            countY = doneY + doneYI + 1;
            countX = doneX + doneXI + 1;
            //Debug.WriteLine(doneY);
            //Debug.WriteLine(doneX);

            var searchRectx = new Rectangle(tile.X - (tile.Height * (doneX - 1)), tile.Y - (tile.Width * (doneY - 1)), tile.Width, tile.Height);
            g.FillRectangle(pgTop, searchRectx);

        }

        public static Bitmap GetAutoFindImage()
        {
            return (Bitmap)Resources.ResourceManager.GetObject("autofind");
        }

    }
}
