﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemakerMaker.Util
{
    /// <summary>
    /// Provides functions to capture the entire screen, or a particular window, and save it to a file.
    /// </summary>
    public class ScreenCapture
    {
        public static ConcurrentQueue<Bitmap> queue = new ConcurrentQueue<Bitmap>();
        public static Task producer;
        public static Bitmap lastCapturedImage;

        public static void StartTask(IntPtr mainWindowHandle, int offsetX, int offsetY)
        {
            if (producer != null)
            {
                return;
            }

            producer = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (queue.Count > 500)
                    {
                        continue;
                    }
                    
                    var bitmap = CaptureApplication(mainWindowHandle, offsetX, offsetY);

                    //if (queue.Count == 0)
                    //{
                    //    queue.Enqueue(bitmap);
                    //    continue;
                    //}
                    //
                    //Bitmap lastCapturedImage;
                    //if (queue.TryPeek(out lastCapturedImage))
                    //{
                    //
                    //    if (!ProcessCapture.CompareMemCmp(bitmap, lastCapturedImage))
                    //    {
                    //        queue.Enqueue(bitmap);
                    //    }
                    //}
                    queue.Enqueue(bitmap);
                    Thread.Sleep(1);
                    Debug.WriteLine(queue.Count);
                }
            });
        }

        public static Bitmap getLastImage()
        {
            Bitmap value;
            if (queue.TryDequeue(out value))
            {
                return value;
            }
            return null;
        }


        /// <summary>
        /// Creates an Image object containing a screen shot of the entire desktop
        /// </summary>
        /// <returns></returns>
        public Image CaptureScreen() 
        {
            return CaptureWindow( User32.GetDesktopWindow() );
        }
        /// <summary>
        /// Creates an Image object containing a screen shot of a specific window
        /// </summary>
        /// <param name="handle">The handle to the window. (In windows forms, this is obtained by the Handle property)</param>
        /// <returns></returns>
        public Image CaptureWindow(IntPtr handle)
        {
            // get te hDC of the target window
            IntPtr hdcSrc = User32.GetWindowDC(handle);
            // get the size
            User32.Rect windowRect = new User32.Rect();
            User32.GetWindowRect(handle,ref windowRect);
            int width = windowRect.right - windowRect.left;
            int height = windowRect.bottom - windowRect.top;
            // create a device context we can copy to
            IntPtr hdcDest = GDI32.CreateCompatibleDC(hdcSrc);
            // create a bitmap we can copy it to,
            // using GetDeviceCaps to get the width/height
            IntPtr hBitmap = GDI32.CreateCompatibleBitmap(hdcSrc,width,height); 
            // select the bitmap object
            IntPtr hOld = GDI32.SelectObject(hdcDest,hBitmap);
            // bitblt over
            GDI32.BitBlt(hdcDest,0,0,width,height,hdcSrc,0,0,GDI32.SRCCOPY);
            // restore selection
            GDI32.SelectObject(hdcDest,hOld);
            // clean up 
            GDI32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle,hdcSrc);
            // get a .NET image object for it
            Image img = Image.FromHbitmap(hBitmap);
            // free up the Bitmap object
            GDI32.DeleteObject(hBitmap);
            return img;
        }

        public Image CaptureWindowFromScreen(Control wnd) {
            int h = wnd.ClientRectangle.Height, w = wnd.ClientRectangle.Width;
            Bitmap bmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            Image img = CaptureScreen();
            Point p1 = wnd.PointToScreen(new Point(0, 0));
            Graphics gr_bmp = Graphics.FromImage(bmp);
            gr_bmp.DrawImage(img, 0, 0, new RectangleF(p1.X, p1.Y, w, h), GraphicsUnit.Pixel);
            return bmp;
        }
        /// <summary>
        /// Captures a screen shot of a specific window, and saves it to a file
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="filename"></param>
        /// <param name="format"></param>
        public void CaptureWindowToFile(IntPtr handle, string filename, ImageFormat format) 
        {
            Image img = CaptureWindow(handle);
            img.Save(filename,format);
        }
        /// <summary>
        /// Captures a screen shot of the entire desktop, and saves it to a file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="format"></param>
        public void CaptureScreenToFile(string filename, ImageFormat format) 
        {
            Image img = CaptureScreen();
            img.Save(filename,format);
        }
      
        /// <summary>
        /// Helper class containing Gdi32 API functions
        /// </summary>
        private class GDI32
        {
            
            public const int SRCCOPY = 0x00CC0020; // BitBlt dwRop parameter
            [DllImport("gdi32.dll")]
            public static extern bool BitBlt(IntPtr hObject,int nXDest,int nYDest,
                int nWidth,int nHeight,IntPtr hObjectSource,
                int nXSrc,int nYSrc,int dwRop);
            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC,int nWidth, 
                int nHeight);
            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hDC);
            [DllImport("gdi32.dll")]
            public static extern bool DeleteDC(IntPtr hDC);
            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);
            [DllImport("gdi32.dll")]
            public static extern IntPtr SelectObject(IntPtr hDC,IntPtr hObject);
        }

        public static Bitmap CaptureApplication(IntPtr mainWindowHandle) {
             var rect = new User32.Rect();
             User32.GetWindowRect(mainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left, rect.top+100, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
            return bmp;
        }
        public static Bitmap CaptureApplication(IntPtr mainWindowHandle, int offsetX, int offsetY) {
            var rect = new User32.Rect();
            User32.GetWindowRect(mainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left + offsetX, rect.top + offsetY, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
            return bmp;
        }

        public static Bitmap CaptureApplication(IntPtr mainWindowHandle, int offsetX, int offsetY, int heightx, int weightx) {
            var rect = new User32.Rect();
            User32.GetWindowRect(mainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var bmp = new Bitmap(width, height, PixelFormat.Format64bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left + offsetX, rect.top + offsetY, 0, 0, new Size(weightx, heightx), CopyPixelOperation.SourceCopy);
            return bmp;
        }
        /// <summary>
        /// Helper class containing User32 API functions
        /// </summary>
        private class User32
        {
            [StructLayout(LayoutKind.Sequential)]
            public struct Rect
            {
                public int left;
                public int top;
                public int right;
                public int bottom;
            }
            [DllImport("user32.dll")]
            public static extern IntPtr GetDesktopWindow();
            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowDC(IntPtr hWnd);
            [DllImport("user32.dll")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd,IntPtr hDC);
            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd,ref Rect rect);
        }
    }
}