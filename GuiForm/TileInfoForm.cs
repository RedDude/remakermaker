﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RemakerMaker.Main;

namespace RemakerMaker
{
    public partial class MainFrame : Form
    {

        public void SetupTileInfoForm(TileInfo t)
        {

            TopNeighborsLayout.Controls.Clear();
            RightNeighborsLayout.Controls.Clear();
            DownNeighborsLayout.Controls.Clear(); 
            LeftNeighborsLayout.Controls.Clear();

            selectedTilePictureBox.Image = tilesHandler.tileImages[t.Id];
            tileIdLabel.Text = t.Id.ToString();

            // foreach (var n in t.TopNeighbor)
            // {
            //     var pbs = new PictureBox
            //     {
            //         Size = new Size(tileX, tileY),
            //         Image = n.TileImage,
            //         Parent = TopNeighborsLayout,
            //         Tag = n
            //     };
            //
            //     pbs.Click += SetupTileInfoBox_Click;
            // }

            // foreach (var n in t.RightNeighbor)
            // {
            //     var pbs = new PictureBox
            //     {
            //         Size = new Size(tileX, tileY),
            //         Image = n.TileImage,
            //         Parent = RightNeighborsLayout,
            //         Tag = n
            //     };
            //     pbs.Click += SetupTileInfoBox_Click;
            // }
            //
            // foreach (var n in t.DownNeighbor)
            // {
            //     var pbs = new PictureBox
            //     {
            //         Size = new Size(tileX, tileY),
            //         Image = n.TileImage,
            //         Parent = DownNeighborsLayout,
            //         Tag = n
            //     };
            //     pbs.Click += SetupTileInfoBox_Click;
            // }
            //
            // foreach (var n in t.LeftNeighbor)
            // {
            //     var pbs = new PictureBox
            //     {
            //         Size = new Size(tileX, tileY),
            //         Image = n.TileImage,
            //         Parent = LeftNeighborsLayout,
            //         Tag = n
            //     };
            //     pbs.Click += SetupTileInfoBox_Click;
            // }

            ignoreOnSearch.Checked = t.IsIgnoredOnSearch;
        }



        private void ignoreOnSearch_Click(object sender, EventArgs e)
        {
            if (selectTileInfo == null) return;
            selectTileInfo.IsIgnoredOnSearch = !selectTileInfo.IsIgnoredOnSearch;
            if (ignoreOnSearch.Checked != selectTileInfo.IsIgnoredOnSearch)
                ignoreOnSearch.Checked = selectTileInfo.IsIgnoredOnSearch;
        }

        private void deleteTileButton_Click(object sender, EventArgs e)
        {
            if (selectTileInfo != null)
            {
                selectTileInfo.IsIgnoredOnSearch = !selectTileInfo.IsIgnoredOnSearch;
            }
        }
    }
}
