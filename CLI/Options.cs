﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using CommandLine;
using CommandLine.Infrastructure;
using RemakerMaker.Main;
using Utf8Json;
using EnumerableExtensions = CSharpx.EnumerableExtensions;

namespace RemakerMaker.CLI
{
    public class Options
    {
        [Option('a', "autofind", Required = false, HelpText = "Set output to verbose messages.")]
        public IEnumerable<string> autofind { get; set; }

        [Option('s', "find-start", Required = false, HelpText = "Set output to verbose messages.")]
        public IEnumerable<string> findStart { get; set; }
        
        [Option('i', "hash", Required = false, HelpText = "Set output to verbose messages.")]
        public string imageToHash { get; set; }
        
        [Option('e', "extract-tiles", Required = false, HelpText = "Set output to verbose messages.")]
        public IEnumerable<string> extractTiles { get; set; }
        
        // Omitting long name, defaults to name of property, ie "--verbose"
        [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
        public bool Verbose { get; set; }

        [Option("stdin",
            Default = false,
            HelpText = "Read from stdin")]
        public bool stdin { get; set; }

        [Value(0, MetaName = "offset", HelpText = "File offset.")]
        public long? Offset { get; set; }

        static public void RunOptions(Options opts)
        {
            var tilesHandler = new TilesHandler();
            if (opts.extractTiles.Any())
            {
                var frame =  int.Parse(opts.extractTiles.ElementAt(0));
                var frameImagePath = opts.extractTiles.ElementAt(1);
                var autoFind = new Bitmap(opts.extractTiles.ElementAt(2));
                var tileSize = Int16.Parse(opts.extractTiles.ElementAt(3));
                var topOffset =  int.Parse(opts.extractTiles.ElementAt(4));
                var bottomOffset =  int.Parse(opts.extractTiles.ElementAt(5));

                var filePaths = new List<string>();
                if(frameImagePath.Contains(".png")) {
                    filePaths.Add(frameImagePath);
                }
                else
                {
                    filePaths.AddRange(
                        Directory.GetFiles(frameImagePath, "*.png",
                            SearchOption.TopDirectoryOnly)
                    );
                }

                var tiledExport = new TiledExport();
                filePaths.ForEach(path =>
                {
                    var frameImage = new Bitmap(path);
                    var autoFindRectangle = tilesHandler.AutoFindGrid(
                        frameImage, autoFind
                    );
                
                    // if (File.Exists("frame-"+frameNumber+".json"))
                    // {
                    //     var fs = File.Open("frame-"+frameNumber+".json", FileMode.Open, FileAccess.Read, FileShare.None);
                    //     var jsonDeserialize = JsonSerializer.Deserialize<ExtractedData>(fs);
                    //     tilesHandler.ExtractedTiles = jsonDeserialize.tiles;
                    // }
                
                    if (!autoFindRectangle.HasValue) return; 
                   
                    var canvas = new Rectangle(0,topOffset, frameImage.Width, frameImage.Height - bottomOffset);
                    var start = tilesHandler.FindGridStart(canvas, autoFindRectangle.Value);
                    var startRect = new Rectangle(start.Value.X, start.Value.Y, tileSize, tileSize);
                    var extractedData = tilesHandler.ExtractTiles(frame, frameImage, canvas, startRect);
                    tilesHandler.SaveTiles("tiles/", extractedData.newTiles);
                    tilesHandler.ExtractedTiles.AddRange(extractedData.newTiles);
                    // var json = JsonSerializer.ToJsonString(extractedData);
                    // File.WriteAllText("frame-"+frame+".json", json);

                    tiledExport.CreateTileSet(extractedData.tiles, "frame-"+frame, tileSize);
                    tiledExport.CreateMap(extractedData.tiles, extractedData.map, "frame-"+frame, tileSize);
                    frame++;
                });
                                                         
             

                // NeighborMaps(tilesHandler, tiledExport, frameNumber);
                                        
                return;
            }
            
            if (opts.autofind.Any())
            {
                var rect = tilesHandler.AutoFindGrid(
                    new Bitmap(opts.autofind.ElementAt(0)), 
                    new Bitmap(opts.autofind.ElementAt(1))
                );
                System.IO.File.WriteAllText("autofind.json", rect.ToString().Replace('=',':').ToLower());
                return;
            }
            
            if (opts.findStart.Any())
            {
                var tile = new TilesHandler();
                // var rect = tile.FindGridStart(
                //     new Bitmap(opts.autofind.ElementAt(0)), 
                //     new Bitmap(opts.autofind.ElementAt(1))
                // );
                // System.IO.File.WriteAllText("autofind.json", rect.ToString().Replace('=',':').ToLower());
                return;
            }
            
            if (!opts.imageToHash.Empty())
            {
                var tile = new TilesHandler();
                var bitmap = new Bitmap(opts.imageToHash);
                var hash = tile.CreateHashId(bitmap);
                // var bitString = BitConverter.ToInt64(hash, 0);  
                File.WriteAllText(hash.ToString(), "");
                // var rect = tile.FindGridStart(
                //     new Bitmap(opts.autofind.ElementAt(0)), 
                //     new Bitmap(opts.autofind.ElementAt(1))
                // );
                // System.IO.File.WriteAllText("autofind.json", rect.ToString().Replace('=',':').ToLower());
                return;
            }
            
            if (opts.autofind.Any())
            {
                var rect = tilesHandler.AutoFindGrid(
                    new Bitmap(opts.autofind.ElementAt(0)), 
                    new Bitmap(opts.autofind.ElementAt(1))
                );
                System.IO.File.WriteAllText("autofind.json", rect.ToString().Replace('=',':').ToLower());
                return;
            }
            
            if (opts.Verbose)
            {
                // Console.WriteLine($"Verbose output enabled. Current Arguments: -v {opts.Verbose}");
                // Console.WriteLine("Quick Start Example! App is in Verbose mode!");
                // Console.ReadLine();
            }
        }

        private static void NeighborMaps(TilesHandler tilesHandler, TiledExport tiledExport, string frameNumber, int size)
        {
            tilesHandler.ExtractedTiles.ForEach(tile =>
            {
                var index = 0;
                EnumerableExtensions.ForEach(tile.neighbors, info =>
                {
                    var map = tilesHandler.MapNeigbors(tile, info);
                    tiledExport.CreateMap(tilesHandler.ExtractedTiles, map, "tile-" + tile.Id + "-" + index, size,
                        "frame-" + int.Parse(frameNumber));
                    index++;
                });
            });
        }

        static public void HandleParseError(IEnumerable<Error> errs)
        {
            Console.WriteLine("Return code= {0}", errs);
        }
    }
}