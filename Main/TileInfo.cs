﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RemakerMaker.Main
{
    public class TileInfo
    {
        public ulong Id { get; set; }
        // public Bitmap TileImage { get; set; }
        public int Count { get; set; }
        public bool IsIgnoredOnSearch { get; set; }
        
        [IgnoreDataMember]
        public HashSet<Color> Colors { get; set; }

        public HashSet<TileNeighborInfo> neighbors { get; set; }
        public ulong Hash { get; set; }
    }
}
