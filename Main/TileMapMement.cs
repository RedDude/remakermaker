﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace RemakerMaker.Main
{
    public class TileMapMement
    {

        public ulong[,] TileMap { get; set; }

        private List<TileInfo> _extractedTiles = new List<TileInfo>();

        public List<TileInfo> ExtractedTiles
        {
            get { return _extractedTiles; }
            set { _extractedTiles = value; }
        }

        public Bitmap ImageSource { get; set; }
    }
}
