﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace RemakerMaker.Main
{
    class MapHandler
    {

        private List<TileMapMement> _tileMapMements = new List<TileMapMement>();

        public bool AddMement(TileMapMement memento)
        {
            TileMapMement lastMemento = null;
            if (  _tileMapMements.Count > 0)
            {
                 lastMemento = _tileMapMements.Last();
            }
            
            if (lastMemento != null)
            {
                if (!ContentEquals(memento.TileMap, lastMemento.TileMap))
                {
                    _tileMapMements.Add(memento);
                    return true;
                }
            }
            else
            {
                _tileMapMements.Add(memento);
                return true;
            }

            return false;
        }

        public TileMapMement GetMemento(int i)
        {
            if (i < 0)
            {
                i = 0;
            }
            return _tileMapMements[i];
        }

        public int MementosCount()
        {
            return _tileMapMements.Count;
        }

        public List<TileMapMement> GetTileMapMement()
        {
            return _tileMapMements;
        }


        public void CleanMementos()
        {
            _tileMapMements = new List<TileMapMement>();
        }

       //private void FillTilesNeighbors(TileMapMement tileMapMement)
       //{
       //    var map = tileMapMement.TileMap;
       //
       //    var rowCount = map.GetLength(0);
       //    var colCount = map.GetLength(1);
       //
       //    for (int row = 0; row < rowCount; row++)
       //    {
       //        for (int col = 0; col < colCount; col++)
       //        {
       //            var pbs = new PictureBox
       //            {
       //                map.ExtractedTiles.Find(t => t.Id == map[row, col]);
       //                //Size = new Size(tileX, tileY),
       //            };
       //
       //        }
       //    }
       //}

        public bool ContentEquals<T>(T[,] arr, T[,] other) where T : IComparable
        {
            if (arr.GetLength(0) != other.GetLength(0) ||
                arr.GetLength(1) != other.GetLength(1))
                return false;
            for (int i = 0; i < arr.GetLength(0); i++)
                for (int j = 0; j < arr.GetLength(1); j++)
                    if (arr[i, j].CompareTo(other[i, j]) != 0)
                        return false;
            return true;
        }
    }
}
