using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using CommandLine.Infrastructure;

namespace RemakerMaker.Main
{
    public class TiledExport
    {
        string tileSetTemplate = 
@"<?xml version=""1.0"" encoding=""UTF-8""?>
<tileset version=""1.2"" tiledversion=""1.3.1"" name=""${name}"" tilewidth=""${size}"" tileheight=""${size}"" tilecount=""${count}"" columns=""0"">
    <grid orientation=""orthogonal"" width=""1"" height=""1""/>
    ${tiles}
</tileset>";

        private string tileTemplate = 
    @"<tile id=""${id}"">
        <image source=""${source}""/>
    </tile>";
        
        string  mapTemplate = 
@"<?xml version=""1.0"" encoding=""UTF-8""?>
    <map version=""1.2"" tiledversion=""1.3.1"" orientation=""orthogonal"" renderorder=""right-down"" compressionlevel=""-1"" width=""${mapWidth}"" height=""${mapHeight}"" tilewidth=""${size}"" tileheight=""${size}"" infinite=""0"" nextlayerid=""2"" nextobjectid=""1"">
    <tileset firstgid=""1"" source=""${tilesetName}""/>
    <layer id=""1"" name=""layer1"" width=""${mapWidth}"" height=""${mapHeight}"">
    <data encoding=""csv"">
${csv}</data>
    </layer>
    </map>";

        public void CreateTileSet(List<TileInfo> tiles, string name, int size)
        {
            var template = new StringBuilder(tileSetTemplate.Clone().ToString());
            var tilesStrings = new StringBuilder("");

            var tileSize = size.ToString();
            var index = 0;
            foreach (var tileInfo in tiles) {
                var currentTileTemplate = new StringBuilder(tileTemplate.Clone().ToString());
                currentTileTemplate.Replace("${id}", index.ToString());
                currentTileTemplate.Replace("${size}", tileSize);
                currentTileTemplate.Replace("${source}", "tiles/tile" + tileInfo.Id + ".png");
                tilesStrings.Append(currentTileTemplate+"\n\t");
                index++;
            }

            template.Replace("${size}", tileSize);
            template.Replace("${name}", name);
            template.Replace("${count}", tiles.Count.ToString());
            template.Replace("${tiles}", tilesStrings.ToString());

            File.WriteAllText(name + ".tsx", template.ToString());
        }
        
        public void CreateMap(List<TileInfo> extractedTiles, ulong[,] map, string name, int size, string tilesetName = "")
        {
            var template = new StringBuilder(mapTemplate.Clone().ToString());

            var tileSize = size.ToString();
            var index = 1;

            var convertedMap = ToMatrix(map);
            var mapString = ToMatrixString(convertedMap,",");
          
            // Debug.WriteLine(mapString.ToString());
            // foreach (var tileInfo in extractedTiles) {
            //     mapString.Replace(tileInfo.Id.ToString(), index.ToString());
            //     index++;
            // }

            template.Replace("${size}", tileSize);
            template.Replace("${name}", name);
            template.Replace("${mapHeight}", map.GetLength(0).ToString());
            template.Replace("${mapWidth}", map.GetLength(1).ToString());
            template.Replace("${tilesetName}", (!tilesetName.Empty() ? tilesetName : name) + ".tsx");
            template.Replace("${csv}", mapString.ToString());

            File.WriteAllText(name + ".tmx", template.ToString());
        }
        
        public StringBuilder ToMatrixString<T>(T[,] matrix, string delimiter = "\t")
        {
            var s = new StringBuilder();
    
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    s.Append(matrix[i, j]);
//                    if(i != matrix.GetLength(0)-1 && j != matrix.GetLength(1) - 1)
                        s.Append(delimiter);
                }
                if(i != matrix.GetLength(0)-1)
                    s.AppendLine();
            }
            
            s.Remove(s.Length - 1, 1);
            return s;
        }
        
        public ulong[,] ToMatrix(ulong[,] matrix)
        {
            var map = new ulong[matrix.GetLength(0), matrix.GetLength(1)];
            var index = (ulong)1; 
            var already = new Dictionary<ulong, ulong>();
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    if (!already.ContainsKey(matrix[i, j]))
                    {
                        already[matrix[i, j]] = index++;
                    }
                    
                    map[i, j] = already[matrix[i, j]];
                }
            }
            return map;
        }
    }
    
    
}