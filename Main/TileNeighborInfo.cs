﻿namespace RemakerMaker.Main
{
    public class TileNeighborInfo
    {
        public int frame;
        public ulong TopNeighbor { get; set; }
        public ulong TopLeftNeighbor { get; set; }
        public ulong LeftNeighbor { get; set; }
        public ulong DownLeftNeighbor { get; set; }
        public ulong DownNeighbor { get; set; }
        public ulong DownRightNeighbor { get; set; }
        public ulong RightNeighbor { get; set; }
        public ulong TopRightNeighbor { get; set; }
    }
}