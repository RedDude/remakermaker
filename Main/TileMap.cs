﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace RemakerMaker.Main
{
    class TileMap
    {
        public Point Position { get; set; }
        public int[] Ids { get; set; }
    }
}
