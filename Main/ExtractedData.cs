﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Utf8Json;

namespace RemakerMaker.Main
{
    public class ExtractedData
    {
        public List<TileInfo> newTiles;
        public List<TileInfo> tiles;
        public ulong[,] map;
        public TileInfo[,] tileInfoMap;
        
        [IgnoreDataMember]
        public List<TileInfo> tempExtractedTiles;

        public ExtractedData(List<TileInfo> newTiles, List<TileInfo> tiles, ulong[,] map, TileInfo[,] tileInfoMap, List<TileInfo> tempExtractedTiles)
        {
            this.newTiles = newTiles;
            this.tiles = tiles;
            this.map = map;
            this.tileInfoMap = tileInfoMap;
            this.tempExtractedTiles = tempExtractedTiles;
        }
    }
}