﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using RemakerMaker.EventBus;
using RemakerMaker.Util;

namespace RemakerMaker.Main
{
    public class TilesHandler
    {
        private List<TileInfo> _extractedTiles = new List<TileInfo>();
        public Dictionary<ulong, Bitmap> tileImages = new Dictionary<ulong, Bitmap>();
        
        private SHA512 hashCreator;

        public TilesHandler()
        {
            hashCreator = new SHA512Managed();
        }
   
        public List<TileInfo> ExtractedTiles
        {
            get { return _extractedTiles; }
            set { _extractedTiles = value; }
        }


        public ExtractedData ExtractTiles(
            int frame,
            Bitmap image,
            Rectangle canvas,
            Rectangle tileStartRectangle)
        {
            var newTiles = new List<TileInfo>();
            var mapTiles = new List<TileInfo>();
            
            canvas.X = 0;
            canvas.Y = 0;

            var picWidth = canvas.Width;
            var picHeight = canvas.Height;
            
            decimal num = (picWidth - tileStartRectangle.Y) / tileStartRectangle.Height;
            var cast = (int)num;

            var numCellsX = cast;

            num = (picHeight - tileStartRectangle.X)/tileStartRectangle.Width;

            cast = (int)num;
            var numCellsY = cast - 1;

            var map = new ulong[numCellsY, numCellsX];
            var tileInfoMap = new TileInfo[numCellsY, numCellsX];

            var tempExtractedTiles = new List<TileInfo>();
            tempExtractedTiles.AddRange(ExtractedTiles);

            var index = (ulong)tempExtractedTiles.Count;
            for (var y = 0; y < numCellsY; y++)
            {
                for (var x = 0; x < numCellsX; x++)
                {
                    try {

                        var searchRect = new Rectangle(
                        x * tileStartRectangle.Width + tileStartRectangle.X, 
                        y * tileStartRectangle.Height + tileStartRectangle.Y, 
                           tileStartRectangle.Width, tileStartRectangle.Height
                        );

                        if (!canvas.Contains(searchRect))
                        {
                            break;
                        }

                        var format = image.PixelFormat;
                        var tileImage = image.Clone(searchRect, format);
                        var hash = CreateHashId(tileImage);
                        var ti = new TileInfo();

                        if (AlreadyExtracted(tempExtractedTiles, hash, ref ti))
                        {
                            if (AlreadyExtracted(tempExtractedTiles, tileImage, tileImages, ref ti, hash))
                            {
                                ti.Count++;
                                if (!mapTiles.Contains(ti))
                                {
                                    mapTiles.Add(ti);
                                }

                                map[y, x] = ti.Id;
                                tileInfoMap[y, x] = ti;
                                continue;
                            }
                        }

                        ++index;
                        tileImages[index] = tileImage;
                        var tileColors = ImageColors(tileImage);
                        ti = new TileInfo
                        {
                            Id = index,
                            Hash = hash,
                            Count = 1,
                            Colors = tileColors,
                            IsIgnoredOnSearch = (tileColors.Count < 2),
                            neighbors = new HashSet<TileNeighborInfo>()
                        };
                        
                        MessageHub.Publish(new NewTile(ti));
                      
                        tempExtractedTiles.Add(ti);
                        newTiles.Add(ti);
                        if (!mapTiles.Contains(ti))
                        {
                            mapTiles.Add(ti);
                        }
                        map[y, x] = ti.Id;
                        tileInfoMap[y, x] = ti;

                    } catch(Exception) {
                        //System.Windows.Forms.MessageBox.Show(y + " x " + x);
                        //throw;
                    }
                   
                }
            }
            
            for (var y = 0; y < numCellsY; y++)
            {
                for (var x = 0; x < numCellsX; x++)
                {
                    var neighbors = FillNeighbors(x, y, numCellsX, numCellsY, map);
                    neighbors.frame = frame;
                    var tileInfo = tileInfoMap[y, x];
                    if(tileInfo != null)
                        tileInfo.neighbors.Add(neighbors);
                }
            }

            return new ExtractedData(newTiles, mapTiles, map, tileInfoMap, tempExtractedTiles);
        }

        public List<TileInfo> ExtractTiles(
            int frame,
            Bitmap image,
            Rectangle canvas,
            Rectangle tileStartRectangle,
            ref TileInfo tileStart)
        {
            var extractedData= ExtractTiles(frame, image, canvas, tileStartRectangle);
                 
            if (_extractedTiles.Count > 0 && extractedData.tiles.Count > 40 && tileStart != null)
            {
                var tooMany = new TooManyTilesExtracted(tileStartRectangle);
                MessageHub.Publish(tooMany);
                if (tooMany.Response)
                {
                    tileStart.IsIgnoredOnSearch = true;
                    return new List<TileInfo>();
                }
            }

            _extractedTiles.AddRange(extractedData.tiles);

            MessageHub.Publish(new NewMapExtractedMessage(
                extractedData.map, 
                extractedData.tempExtractedTiles,
                image)
            );

            return extractedData.tiles;
        }
            
        public static bool AlreadyExtracted(
            List<TileInfo> extractTilesBitmaps, 
            Bitmap newTile, 
            Dictionary<ulong, Bitmap> tileImages, 
            ref TileInfo ti,
            ulong hash = 0)
        {
            var already = false;

            // foreach (var tile in extractTilesBitmaps)
            // {
            //     if (ProcessCapture.CompareMemCmp(tileImages[tile.Id], newTile))
            //     {
            foreach (
                var tile in extractTilesBitmaps.Where(
                    tile => 
                        (hash != 0 && tile.Hash == hash || hash == 0) &&
                        ProcessCapture.CompareMemCmp(tileImages[tile.Id], newTile))
            ) {
                    already = true;
                    ti = tile;
                    break;
            }

            return already;
           
        }
        
        public bool AlreadyExtracted(
            List<TileInfo> extractTilesBitmaps, 
            ulong hash, 
            ref TileInfo ti)
        {
            var already = false;
            foreach(var tile in extractTilesBitmaps) {
                if (tile.Hash != hash) continue;
                already = true;
                ti = tile;
                break;
            }
            return already;
        }
        
        public TileNeighborInfo FillNeighbors(int x, int y, int width, int height, ulong[,] map)
        {
            var tileNeighborInfo = new TileNeighborInfo();

            if (y > 0 && x < width - 2)
                tileNeighborInfo.TopRightNeighbor = map[y - 1, x + 1];
            
            if (y > 0)
                tileNeighborInfo.TopNeighbor = map[y - 1, x];
            
            if (x > 0 && y > 0)
                tileNeighborInfo.TopLeftNeighbor = map[y - 1, x - 1];
                        
            if (x > 0)
                tileNeighborInfo.LeftNeighbor = map[y, x - 1];

            if (y < height - 2 && x > 0)
                tileNeighborInfo.DownLeftNeighbor = map[y + 1, x - 1];
            
            if (y < height - 2)
                tileNeighborInfo.DownNeighbor = map[y + 1, x];
            
            if (y < height - 2 && x < width - 2)
                tileNeighborInfo.DownRightNeighbor = map[y + 1, x + 1];
            
            if (x < width - 2)
                tileNeighborInfo.RightNeighbor = map[y, x + 1];

            return tileNeighborInfo;
        }

        public Rectangle? FindGrid(Bitmap image, List<TileInfo> extractTilesBitmaps = null)
        {
            if (extractTilesBitmaps == null)
            {
                extractTilesBitmaps = _extractedTiles;
            }

            foreach (var tile in extractTilesBitmaps)
            {
                if (tile.IsIgnoredOnSearch)
                {
                    continue;
                }

                var find = AutoFindGrid(image, tileImages[tile.Id]);
                if (find != null)
                {
                    MessageHub.Publish(new FindGridPositionChanged(tile));
                    return find;
                }
              
            }
            return null;
        }

        public Rectangle? AutoFindGrid(Bitmap image, Bitmap tile)
        {
            var rect = SearchBitmap(tile, image, 0);
            if (!rect.IsEmpty)
            {
                return rect;
            }
            return null;
        }

        public Rectangle? FindGridStart(Rectangle pic, Rectangle tile)
        {
            pic.X = 0;
            pic.Y = 0;

            var i = 0;

            var y = 1;
            var yi = 1;

            var x = 1;
            var xi = 1;

            var doneY = 0;
            var doneYI = 0;

            var doneX = 0;
            var doneXI = 0;

            var countY = 0;
            var countX = 0;

            while (i < 20)
            {
                i++;

                if (doneY == 0)
                {
                    var searchRect = new Rectangle(tile.X, tile.Y - (tile.Height * y), tile.Width, tile.Height);

                    y++;

                    if (!pic.Contains(searchRect))
                    {
                        doneY = --y;
                    }
                }

                if (doneYI == 0)
                {
                    var searchRect = new Rectangle(tile.X, tile.Y + (tile.Height * yi), tile.Width, tile.Height);

                    yi++;
                    if (!pic.Contains(searchRect))
                    {
                        doneYI = --yi;
                    }
                }

                if (doneX == 0)
                {
                    var searchRect = new Rectangle(tile.X - (tile.Height * x), tile.Y, tile.Width, tile.Height);

                    x++;
                    if (!pic.Contains(searchRect))
                    {
                        doneX = --x;
                    }
                }

                if (doneXI == 0)
                {
                    var searchRect = new Rectangle(tile.X + (tile.Height * xi), tile.Y, tile.Width, tile.Height);

                    xi++;
                    if (!pic.Contains(searchRect))
                    {
                        doneXI = --xi;

                    }
                }

                if (doneYI > 0 && doneY > 0 && doneXI > 0 && doneX > 0)
                {
                    break;
                }

            }

            var searchRectx = new Rectangle(tile.X - (tile.Height * (doneX - 1)), 
                                            tile.Y - (tile.Width * (doneY - 1)), 
                                            tile.Width, tile.Height);
            return searchRectx;
        }

        public void SaveTiles(string savePath, List<TileInfo> extractTilesBitmaps = null)
        {
            if (extractTilesBitmaps == null)
            {
                extractTilesBitmaps = _extractedTiles;
            }

            foreach (var tile in extractTilesBitmaps)
            {
                Bitmap imaget = tileImages[tile.Id];
                // ReplaceColor(ref imaget, Color.FromArgb(57, 148, 189, 57), Color.FromArgb(0, 0, 0, 0));
                imaget.Save(savePath +"tile"+ tile.Id + ".png", ImageFormat.Png);    
            }
        }

        private Rectangle SearchBitmap(Bitmap smallBmp, Bitmap bigBmp, double tolerance)
        {
            BitmapData smallData =
              smallBmp.LockBits(new Rectangle(0, 0, smallBmp.Width, smallBmp.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format24bppRgb);
            BitmapData bigData =
              bigBmp.LockBits(new Rectangle(0, 0, bigBmp.Width, bigBmp.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format24bppRgb);

            int smallStride = smallData.Stride;
            int bigStride = bigData.Stride;

            int bigWidth = bigBmp.Width;
            int bigHeight = bigBmp.Height - smallBmp.Height + 1;
            int smallWidth = smallBmp.Width * 3;
            int smallHeight = smallBmp.Height;

            Rectangle location = Rectangle.Empty;
            int margin = Convert.ToInt32(255.0 * tolerance);

            unsafe
            {
                byte* pSmall = (byte*)(void*)smallData.Scan0;
                byte* pBig = (byte*)(void*)bigData.Scan0;

                int smallOffset = smallStride - smallBmp.Width * 3;
                int bigOffset = bigStride - bigBmp.Width * 3;

                bool matchFound = true;

                for (int y = 0; y < bigHeight; y++)
                {
                    for (int x = 0; x < bigWidth; x++)
                    {
                        byte* pBigBackup = pBig;
                        byte* pSmallBackup = pSmall;

                        //Look for the small picture.
                        for (int i = 0; i < smallHeight; i++)
                        {
                            int j = 0;
                            matchFound = true;
                            for (j = 0; j < smallWidth; j++)
                            {
                                //With tolerance: pSmall value should be between margins.
                                int inf = pBig[0] - margin;
                                int sup = pBig[0] + margin;
                                if (sup < pSmall[0] || inf > pSmall[0])
                                {
                                    matchFound = false;
                                    break;
                                }

                                pBig++;
                                pSmall++;
                            }

                            if (!matchFound) break;

                            //We restore the pointers.
                            pSmall = pSmallBackup;
                            pBig = pBigBackup;

                            //Next rows of the small and big pictures.
                            pSmall += smallStride * (1 + i);
                            pBig += bigStride * (1 + i);
                        }

                        //If match found, we return.
                        if (matchFound)
                        {
                            location.X = x;
                            location.Y = y;
                            location.Width = smallBmp.Width;
                            location.Height = smallBmp.Height;
                            break;
                        }
                        //If no match found, we restore the pointers and continue.
                        else
                        {
                            pBig = pBigBackup;
                            pSmall = pSmallBackup;
                            pBig += 3;
                        }
                    }

                    if (matchFound) break;

                    pBig += bigOffset;
                }
            }

            bigBmp.UnlockBits(bigData);
            smallBmp.UnlockBits(smallData);

            return location;
        }
        
        public byte[] CreateHash(Bitmap bmp)
        {
            var size = bmp.Width;
            var rawImageData = new byte[size * size];
            var bmpd = bmp.LockBits(new Rectangle(0, 0, size, size), 
                ImageLockMode.ReadOnly, 
                PixelFormat.Format32bppArgb);
            Marshal.Copy(bmpd.Scan0, rawImageData, 0, size * size);
            bmp.UnlockBits(bmpd);
            return hashCreator.ComputeHash(rawImageData);
            
            // byte[] rawImageData = new byte[30 * 30];
            // BitmapData bmpd = simpleImage.LockBits(new Rectangle(0, 0, 30, 30), 
            //     ImageLockMode.ReadOnly, 
            //     PixelFormat.Format32bppArgb);
            // Marshal.Copy(bmpd.Scan0, rawImageData, 0, 30 * 30);
            // simpleImage.UnlockBits(bmpd);
            //
            // var btImage = new byte[1];
            // btImage = (byte[])imageConverter.ConvertTo(bmp1, btImage.GetType());
            //
            // return shaManaged.ComputeHash(btImage);
        }

        public bool CompareHash(byte[] hash, byte[] hash2)
        {
            for (var i = 0; i < hash.Length && i < hash2.Length; i++)
            {
                if (hash[i] != hash2[i])
                    return false;
            }

            return true;
        }

        public ulong CreateHashId(Bitmap bmp)
        {
            var hash = CreateHash(bmp);
            return BitConverter.ToUInt64(hash, 0);  
        }

        public static HashSet<Color> ImageColors(Bitmap tile)
        {
            var colors = new HashSet<Color>();

            for (var y = 0; y < tile.Size.Height; y++)
            {
                for (var x = 0; x < tile.Size.Width; x++)
                {
                    colors.Add(tile.GetPixel(x, y));
                }
            }

            return colors;
        }

        public ulong[,] MapNeigbors(TileInfo tileInfo, TileNeighborInfo neighborInfo)
        {
            var map = new ulong[3, 3];

            map[0, 0] = neighborInfo.TopLeftNeighbor;
            map[0, 1] = neighborInfo.TopNeighbor;
            map[0, 2] = neighborInfo.TopRightNeighbor;
            map[1, 0] = neighborInfo.LeftNeighbor;
            map[1, 1] = tileInfo.Id;
            map[1, 2] = neighborInfo.RightNeighbor;
            map[2, 0] = neighborInfo.DownLeftNeighbor;
            map[2, 1] = neighborInfo.DownNeighbor;
            map[2, 2] = neighborInfo.DownRightNeighbor;

            return map;
        }
        
        private void ReplaceColor(ref Bitmap bmp, Color oldColor, Color newColor)
        {
            var lockedBitmap = new FastBitmap(bmp);
            lockedBitmap.LockBitmap();
        
            var old = new PixelData(oldColor);
            var toReplace = new PixelData(newColor);
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    if (lockedBitmap.GetPixel(x, y).Equals(old))
                    {
                        lockedBitmap.SetPixel(x, y, toReplace);
                    }
                }
            }
            lockedBitmap.UnlockBitmap();
        }
    }
}
