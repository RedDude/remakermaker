﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemakerMaker.Main
{
    class MapInfo
    {
        public int Id { get; set; }
        public List<TileMap> MapTiles { get; set; }
        public TileMap Background { get; set; }
    }
}
