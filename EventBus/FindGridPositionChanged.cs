﻿using RemakerMaker.Main;
using TinyMessenger;

namespace RemakerMaker.EventBus
{

    public class FindGridPositionChanged
      : TinyMessageBase
    {
        public FindGridPositionChanged(TileInfo sender)
            : base(sender)
        {
        }
    }
}
