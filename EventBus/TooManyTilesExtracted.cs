﻿using System;
using System.Drawing;
using RemakerMaker.Main;
using TinyMessenger;

namespace RemakerMaker.EventBus
{

    public class TooManyTilesExtracted
      : TinyMessageBase
    {
        public bool Response { get; set; }

        public TooManyTilesExtracted(Rectangle tileStart)
            : base(tileStart)
        {
        }
    }
}
