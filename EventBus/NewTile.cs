﻿using RemakerMaker.Main;
using TinyMessenger;

namespace RemakerMaker.EventBus
{
    public class NewTile
        : TinyMessageBase
    {
        public NewTile(TileInfo sender)
            : base(sender)
        {
        }
    }

}
