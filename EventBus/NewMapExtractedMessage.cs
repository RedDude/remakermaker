﻿using System.Collections.Generic;
using System.Drawing;
using RemakerMaker.Main;
using TinyMessenger;

namespace RemakerMaker.EventBus
{

    public class NewMapExtractedMessage
      : TinyMessageBase
    {
        private TileMapMement _tileMapMement;
        public NewMapExtractedMessage(ulong[,] sender, List<TileInfo> tempExtractedTiles, Bitmap imageSource)
            : base(sender)
        {
            _tileMapMement = new TileMapMement
            {
                TileMap = sender,
                ExtractedTiles = tempExtractedTiles,
                ImageSource = imageSource
            };
        }

        public TileMapMement GetMapExtractedMemento()
        {
            return _tileMapMement; 
        }
    }
}
