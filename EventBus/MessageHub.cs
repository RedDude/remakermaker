﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;

namespace RemakerMaker.EventBus
{
    class MessageHub
    {
        private TinyMessengerHub hub;
        private static MessageHub instance_ = null;

        private static MessageHub instance
        {
            get
            {
                if (instance_ == null)
                {
                    instance_ = new MessageHub {hub = new TinyMessengerHub()};
                }

                return instance_;
            }
        }

        public static TinyMessageSubscriptionToken Subscribe<TMessage>(Action<TMessage> handler)
            where TMessage : class, ITinyMessage
        {
            return instance.hub.Subscribe(handler);
        }

        public static void Publish<TMessage>(TMessage message)
            where TMessage : class, ITinyMessage
        {
            instance.hub.Publish(message);
        }

        public static void Unsubscribe<TMessage>(TinyMessageSubscriptionToken token)
            where TMessage : class, ITinyMessage
        {
            instance.hub.Unsubscribe<TMessage>(token);
        }

   

    }
}
