﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RemakerMaker.EventBus;
using RemakerMaker.Main;
using RemakerMaker.Util;

namespace RemakerMaker
{
    public partial class MainFrame : Form
    {
        private Timer _mainLoop;

        private TilesHandler tilesHandler = new TilesHandler();
        private MapHandler _mapHandler = new MapHandler();

        private int offsetX;
        private int offsetY;

        private int padX;
        private int padY;

        private int tileX;
        private int tileY;

        private int picWidth;
        private int picHeight;

        private Process[] proccess;

        private Bitmap bitmap;
        private Bitmap lastCapturedImage;

        private Bitmap autoFindImage;

        private Rectangle? foundRectangle = null;
        private Rectangle? lastFoundRectangle = null;
        private TileInfo lastFoundTile = null;


        private TileInfo selectTileInfo = null;

        private const string proccessName = "snes9x-x64";

        private bool isGridTileMementoInitalized = false;

        public MainFrame()
        {
            InitializeComponent();

            autoFindImage = FormHelper.GetAutoFindImage();
            autoFindPictureBox.Image = autoFindImage;

            try
            {
                proccess = Process.GetProcessesByName(proccessName);
                ProcessCapture.ProcessAlwaysOnTop(proccess.First());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           

            MessageHub.Subscribe<NewTile>(m =>
            {
                var tile = m.Sender as TileInfo;
                //Debug.WriteLine(tile.Id);
            });

            MessageHub.Subscribe<FindGridPositionChanged>(m =>
            {
                var tile = m.Sender as TileInfo;
                var controls = this.flowLayoutPanel1.Controls.OfType<PictureBox>();
                
                lastFoundTile = tile;
                autoFindPictureBox.Image = tilesHandler.tileImages[tile.Id];
            });


            MessageHub.Subscribe<NewMapExtractedMessage>(m =>
            {
                var memento = m.GetMapExtractedMemento();
                var added = _mapHandler.AddMement(memento);

                playerBar.Maximum = _mapHandler.MementosCount();

                if (added && playerBar.Value + 1 == playerBar.Maximum)
                {
                    playerBar.Value += 1;
                }

            });

            MessageHub.Subscribe<TooManyTilesExtracted>(m =>
            {
                if (autoIgnoreTooManyTiles.Checked)
                {
                    m.Response = true;
                    return;
                }

                _mainLoop.Stop();
                m.Response = (MessageBox.Show(
                    "Too many new tiles, this probably means that tiles got offgrid or is a new map. do you want ignore the tile used to find the grid?",
                    "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == DialogResult.Yes);
                _mainLoop.Start();
            });

            Console.WriteLine();
        }

        #region Timer

        private void _Load(object sender, EventArgs e)
        {

            _mainLoop = new Timer {Interval = (1)};
            _mainLoop.Tick += new EventHandler(MyTimer_Tick);
            _mainLoop.Start();
        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {
            if (proccess != null)
            {
               //ScreenCapture.StartTask(proccess[0].MainWindowHandle, padX, padY);
               //bitmap = ScreenCapture.getLastImage();
               
 
                bitmap = ScreenCapture.CaptureApplication(proccess[0].MainWindowHandle, padX, padY);
                if (bitmap == null)
                    return;

                mainPictureBox.Image = bitmap;

                //if (ProcessCapture.CompareMemCmp(bitmap, lastCapturedImage))
                //{
                //    return;
                //}

                lastCapturedImage = bitmap;
            }
            mainPictureBox.Refresh();

            if (autoExtractCheckBox.Checked && foundRectangle != null)
            {
                Bitmap pic = new Bitmap(mainPictureBox.Image);

                lastFoundRectangle = tilesHandler.FindGrid(pic);

                if (foundRectangle != null && lastFoundRectangle != null)
                {

                    if (!foundRectangle.Value.IntersectsWith((Rectangle)lastFoundRectangle))
                    {
                        Debug.WriteLine("changed");
                    }


                    if (autoExtractCheckBox.Checked)
                    {
                        var start = tilesHandler.FindGridStart(mainPictureBox.Bounds,
                            (Rectangle) foundRectangle);
                        ExtractTiles((Rectangle) start);
                    }
                }

            }
        }

        #endregion


        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            var picWidth = Convert.ToInt16(fpicWidth.Text);
            var picHeight = Convert.ToInt16(fpicHeight.Text);

            tileX = Convert.ToInt16(ftileX.Text);
            tileY = Convert.ToInt16(ftileY.Text);

            mainPictureBox.Width = picHeight;
            mainPictureBox.Height = picWidth;

            if (foundRectangle == null)
            {
                offsetX = Convert.ToInt16(foffsetX.Text);
                offsetY = Convert.ToInt16(fOffsetY.Text);

                padX = Convert.ToInt16(fPadX.Text);
                padY = Convert.ToInt16(fPadY.Text);

                FormHelper.DrawGrid(ref e, picWidth, picHeight, tileY, tileX, offsetY, offsetX);

            }
            else
            {
                FormHelper.DrawDetectRet(ref e, (Rectangle) foundRectangle);

                Bitmap pic = new Bitmap(mainPictureBox.Image);
                var rect = tilesHandler.FindGrid(pic);
                //var rect = tilesHandler.AutoFindGrid(pic, FormHelper.GetAutoFindImage());
                if (rect != null)
                {
                    foundRectangle = rect;
                    FormHelper.DrawFoundRectangleGrid(ref e, picWidth, picHeight, tileY, tileX,
                        (Rectangle) foundRectangle);

                    FormHelper.FindStartGrid(ref e, pic, mainPictureBox.Bounds, (Rectangle) foundRectangle);
                }
            }

        }

        private int frame = 0;
        private void ExtractTiles(Rectangle start)
        {
            Stopwatch st = new Stopwatch();
            st.Start();

            List<TileInfo> tiles = tilesHandler.ExtractTiles(++frame, bitmap, mainPictureBox.Bounds, start, ref lastFoundTile);
            st.Stop();
            finshTime.Text = st.ElapsedMilliseconds.ToString();

            //flowLayoutPanel1.Controls.Clear();
            PictureBox[] pbs = new PictureBox[tiles.Count];

            for (int i = 0; i < tiles.Count; i++)
            {
                pbs[i] = new PictureBox();
                pbs[i].Size = new Size(tileX, tileY);
                pbs[i].Image = tilesHandler.tileImages[tiles[i].Id];
                pbs[i].Parent = this.flowLayoutPanel1;
                pbs[i].Tag = tiles[i];
                pbs[i].Click += SetupTileInfoBox_Click;
            }

            totalExtractedLabel.Text = tilesHandler.ExtractedTiles.Count.ToString();
        }

        #region Events

        private void extract_Click(object sender, EventArgs e)
        {
            Rectangle start = new Rectangle(offsetX, offsetY, tileX, tileY);
            ExtractTiles(start);
        }

        private void SetupTileInfoBox_Click(object sender, EventArgs e)
        {
            var pb = sender as PictureBox;

            var t = pb.Tag as TileInfo;

            selectTileInfo = t;
            SetupTileInfoForm(t);

          
            //t.IsIgnoredOnSearch = true;
            //tilesHandler.ExtractedTiles.Remove(pb.Tag as TileInfo);

            //pb.Hide();
        }

        private void save_Click(object sender, EventArgs e)
        {
            if (tilesHandler.ExtractedTiles.Count > 0)
            {
                var path = @"C:\Users\rbueno\Pictures\"; //Application.StartupPath 

                tilesHandler.SaveTiles(path + @"\tiles\");
            }
        }

        private void clean_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            foundRectangle = null;
            tilesHandler.ExtractedTiles = new List<TileInfo>();
            _mapHandler.CleanMementos();
            playerBar.Maximum = 0;
            isGridTileMementoInitalized = false;

        }

        private void findGrid_Click(object sender, EventArgs e)
        {
            Bitmap pic = new Bitmap(mainPictureBox.Image);
            var rect = tilesHandler.FindGrid(pic);
            if (rect != null)
            {
                foundRectangle = rect;
                Debug.WriteLine("found");
            }
        }

        private void autoFindGrid_Click(object sender, EventArgs e)
        {
            Bitmap pic = new Bitmap(mainPictureBox.Image);
            var rect = tilesHandler.AutoFindGrid(pic, autoFindImage);
            if (rect != null)
            {
                foundRectangle = rect;
                var startRectangle = tilesHandler.FindGridStart(mainPictureBox.Bounds, (Rectangle) foundRectangle);
                ExtractTiles((Rectangle) startRectangle);

                Debug.WriteLine("found");
            }
            else
            {
                Debug.WriteLine("no good!");
            }
        }

        #endregion


        private void playerBar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar slider = sender as TrackBar;

            var i = slider.Value - 1;
            var momento = _mapHandler.GetMemento(i);
            setFormToMemento(momento);

        }

        private void setFormToMemento(TileMapMement tileMapMement)
        {
            var map = tileMapMement.TileMap;
            if (!isGridTileMementoInitalized)
            {
                setupGridTileMemento(tileMapMement);
            }

            currentMap.Clear();
            var rowCount = map.GetLength(0);
            var colCount = map.GetLength(1);
            for (int row = 0; row < rowCount; row++)
            {
                for (int col = 0; col < colCount; col++)
                {
                      var picBox = gridTileMemento.GetControlFromPosition(col, row) as PictureBox;
                    if (picBox != null && map[row, col] != 0)
                    {
                        var tileInfo = tilesHandler.ExtractedTiles.Find(t => t.Id == map[row, col]);
                        picBox.Image = tilesHandler.tileImages[tileInfo.Id];;
                    }


                    if (show2DArray.Checked)
                    {
                        currentMap.AppendText(String.Format("{0} ", map[row, col].ToString("D3")));
                    }

                }
                if (show2DArray.Checked)
                {
                    currentMap.AppendText(Environment.NewLine);
                }

               
            }
        }


        private void setupGridTileMemento(TileMapMement tileMapMement)
        {
            var map = tileMapMement.TileMap;
            
            var rowCount = map.GetLength(0);
            var colCount = map.GetLength(1);

            gridTileMemento.ColumnCount = colCount;
            gridTileMemento.RowCount = rowCount;
            for (int row = 0; row < rowCount; row++)
            {
                for (int col = 0; col < colCount; col++)
                {
                    var pbs = new PictureBox
                    {
                        BackColor = Color.GreenYellow,
                        Margin = new Padding(0),
                        Padding = new Padding(0)
                        //Size = new Size(tileX, tileY),
                    };
                    gridTileMemento.Controls.Add(pbs, col, row);

                }
            }
            isGridTileMementoInitalized = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var i = playerBar.Value - 1;
            var memento = _mapHandler.GetMemento(i);
            var lastMemento = _mapHandler.GetMemento(i-1);

            var nm = memento.TileMap;
            var lm = lastMemento.TileMap;
            FindROI(nm, lm);

        }


        public void FindROI(ulong[,] nm, ulong[,] lm)//, Font font)
        {
            var rowCount = nm.GetLength(0);
            var colCount = nm.GetLength(1);

            var pointsTopLeft = new List<Point>();
            var pointsTopRight = new List<Point>();
            var pointsBottomLeft = new List<Point>();
            var pointsBottomRight = new List<Point>();

            for (int row = 0; row < rowCount; row++)
            {
                for (int col = 0; col < colCount; col++)
                {
                    if (nm[row, col] == lm[0, 0])
                    {
                        pointsTopLeft.Add(new Point(row, col));
                    }

                    if (nm[row, col] == lm[0, colCount - 1])
                    {
                        pointsTopRight.Add(new Point(row, col));
                    }

                    if (nm[row, col] == lm[rowCount - 1, 0])
                    {
                        pointsBottomLeft.Add(new Point(row, col));
                    }

                    if (nm[row, col] == lm[rowCount - 1, colCount - 1])
                    {
                        pointsBottomRight.Add(new Point(row, col));
                    }

                }
            }

            for (int p = 0; p < pointsTopLeft.Count; p++)
            {
                var x = 0;
                var y = 0;

                for (int row = pointsTopLeft[p].Y; row < rowCount; row++)
                {
                    y = 0;
                    for (int col = pointsTopLeft[p].X; col < colCount; col++)
                    {

                        Color tileColor = Color.Red;
                        if (nm[x, y] == lm[row, col])
                        {
                            tileColor = Color.Black;
                        }

                        y++;
                    }
                    x++;
                }
            }




            for (int p = 0; p < pointsTopRight.Count; p++)
            {
                var x = 0;
                var y = 0;

                for (int row = pointsTopRight[p].Y; row < rowCount; row++)
                {
                    for (int col = 0; col < pointsTopRight[p].X; col++)
                    {
                        y = (colCount - pointsTopRight[p].Y) + col;
                        if (y >= colCount)
                        {
                            break;
                        }

                        Color tileColor = Color.Red;
                        if (nm[x, y] == lm[row, col])
                        {
                            tileColor = Color.Black;
                        }

                    }
                    x++;
                }
            }


        }



        public static void AppendText(RichTextBox box, string text, Color color)//, Font font)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            //box.SelectionFont = font;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

    }


}
